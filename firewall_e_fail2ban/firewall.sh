#!/bin/bash

#limpa as regras de firewall anteriores
iptables -F
iptables -t nat -F
iptables -t mangle -F
iptables -X

#aplicando politica ao firewall
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT ACCEPT

#liberando conexões via interface de loopback
iptables -A INPUT -i lo -j ACCEPT

#liberando conexões da LAN
iptables -A INPUT -s ***FAIXA_IP_LAN/MASK*** -j ACCEPT

#liberando DNS
iptables -A INPUT -p udp --dport 53 -s ***IP_SERVER_DNS*** -j ACCEPT

#liberando acesso SSH para um ip especifico
iptables -A INPUT -p tcp --dport 22 -s ***IP_COM_ACESSO_LIBERADO*** -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

#liberando range de portas para trafego de voz
iptables -A INPUT -p udp --match multport --dports 8000:65000 -j ACCEPT

#liberando portas 5060 a 5065 para registro de contas SIP externas a LAN
#iptables -A INPUT -p tcp --match multport --dports 5060:5065 -j ACCEPT

#liberando porta 4569 para registro de contas IAX2 externas a LAN
#iptables -A INPUT -p udp --dport 4569 -j ACCEPT

#liberando acesso ao Server XMPP com Openfire a um IP especifico
#iptables -A INPUT -p tcp --dport 5222 -s ***IP_COM_ACESSO_LIBERADO*** -j ACCEPT

#liberando demais servicos para funcionamento do XMPP com Openfire
#iptables -A INPUT -p tcp --dport 5223 -j ACCEPT
#iptables -A INPUT -p tcp --dport 7777 -j ACCEPT
#iptables -A INPUT -p udp --match multport --dports 10000:20000 -j ACCEPT

# Liberar protocolo T38 para uso de fax
#iptables -A INPUT -p udp --match multport --dports 4000:4999 -j ACCEPT

#reinicializando fail2ban
service fail2ban restart

#retornando mensagem do firewall
echo "firewall carregado com sucesso"
