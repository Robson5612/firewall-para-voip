# firewall para VOIP

Esse é um exemplo de firewall que pode ser usado em servidores VOIP uma vez que estamos liberando as principais portas para o serviço de telefonia

O firewall é um shell capaz de manipular portas através do iptables, sendo necessário adaptar o mesmo a seu ambiente de rede.

Visto que o arquivo firewall.sh não defende o servidor de ataques do tipo brute force estou adicionando a documentação o arquivo jail.conf com as chains asterisk e sshd configuradas para bloquear qualquer IP que tente acesso com credenciais erradas após 5 tentativas.

O arquivo jail.conf deve substituir o arquivo original do fail2ban, no diretorio /etc/fail2ban/

Para instalação do fail2ban recomendo o artigo https://www.vivaolinux.com.br/dica/Fail2ban-Aprendendo-a-instalar-e-configurar


para configuração do script firewall.sh basta seguir os passos abaixo:

1) Na linha 18, adicionar o endereço de network da LAN;

2) Na linha 21, adicionar o endereço do DNS server caso haja um na rede, se preferir usar o do google 8.8.8.8 e 8.8.4.4;

3) Na linha 24, adicionar o endereço de IP que poderá acessar o servidor via ssh pela porta 22;

4) Para ativar demais serviços como adicionar uma conta SIP ou IAX2 externa a rede LAN basta retirar os comentarios das linhas 30 para SIP e 33 para IAX2;

5) Para liberar o serviço de mensagens XMPP com Openfire descomentar a linha 36 e adicionar o IP que podera ter acesso ao server e tambem descomentar as linhas 39, 40 e 41;

6) Para utilização de fax descomentar a linha 44;


OBS: O upload do firewall não exime as configurações que devem ser feitas no roteador ou firewall da rede, visto que o script é um firewall que funciona exclusivamente para o IPBX
